package aoc2021

fun main() {
    val days = 256
    var school = AOCInputReader.readDay(6).first()
        .split(",").map { it.toInt() }.toIntArray()
        .groupBy { it }.mapValues { it.value.size.toLong() }
    println("Initial State: $school")

    for (day in 1..days) {
        val newSchool = school.mapKeys { it.key - 1 }.toMutableMap()

        (-1..8).forEach { i ->
            val count = newSchool[i] ?: 0
            if (i < 0) {
                newSchool[8] = (newSchool[8] ?: 0) + count
                newSchool[6] = (newSchool[6] ?: 0) + count
                newSchool.remove(i)
            }
        }

        school = newSchool
        println("After %2d days: $school".format(day))
    }

    val total = school.map { it.value }.sum()
    println("Total: $total")
}
