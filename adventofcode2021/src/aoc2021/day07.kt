package aoc2021

import kotlin.math.abs
import kotlin.math.ceil

fun main() {
    var input = AOCInputReader.readDay(7).first()
        .split(",").map { it.toInt() }
//    println("Input: ${input.joinToString(",")}")

    val average = ceil(input.average()).toInt()
    println("Average: $average")

    val costs = HashMap<Int, Int>()
    for (i in input.minOf { it }..average) {
        costs[i] = input.sumOf { abs(i - it).downTo(0).sum() }
    }

    println(costs)
    println(costs.minByOrNull { it.value })
}
