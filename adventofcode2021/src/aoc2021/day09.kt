package aoc2021

data class Point(val x: Int, val y: Int, val height: Int) {
    companion object {
        fun fromHeightMapOrNull(heightMap: List<List<Int>>, x: Int, y: Int): Point? {
            val height = heightMap.getOrNull(y)?.getOrNull(x) ?: return null
            return Point(x, y, height)
        }
    }

    override fun toString(): String {
        return "($x, $y) $height"
    }
}

fun getAdjacents(heightMap: List<List<Int>>, x: Int, y: Int): List<Point> {
    return arrayOf(
        Point.fromHeightMapOrNull(heightMap, x - 1, y),
        Point.fromHeightMapOrNull(heightMap, x + 1, y),
        Point.fromHeightMapOrNull(heightMap, x, y - 1),
        Point.fromHeightMapOrNull(heightMap, x, y + 1),
    ).filterNotNull()
}

fun getBasin(heightMap: List<List<Int>>, source: Point): Int {
    val basinPoints = getBasinPoints(heightMap, source, ArrayList()) + source

    return basinPoints.size
}

fun getBasinPoints(heightMap: List<List<Int>>, source: Point, ignores: ArrayList<Point>): List<Point> {
    ignores.add(source)

    val basinPoints = ArrayList<Point>()
    val adjacents = getAdjacents(heightMap, source.x, source.y)
    for (i in adjacents.indices) {
        if (ignores.contains(adjacents[i])) continue
        if (adjacents[i].height == 9) continue
        if (adjacents[i].height < source.height) continue

        basinPoints.add(adjacents[i])

        val childBasinPoints = getBasinPoints(heightMap, adjacents[i], ignores)
        basinPoints.addAll(childBasinPoints)
    }

    return basinPoints
}

fun main() {
    val heightMap = AOCInputReader.readDay(9)
        .map { row -> row.toCharArray().map { it.toString().toInt() } }

    val lows = ArrayList<Int>()
    val basins = ArrayList<Int>()
    for (y in heightMap.indices) {
        for (x in heightMap[y].indices) {
            val current = heightMap[y][x]

            val adjacents = getAdjacents(heightMap, x, y)
            if (adjacents.minOf { it.height } > current) {
                lows.add(current)
            }

            basins.add(getBasin(heightMap, Point(x, y, current)))
        }
    }

    println(lows.joinToString(","))
    println(lows.sumOf { it + 1 })

    basins.sortBy { -it }
    println(basins)
    println(basins.slice(0..2).fold(1) { total, item -> total * item })
}
